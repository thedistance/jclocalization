//
//  UIButton+JCLocalization.swift
//  JCLocalization
//
//  Created by Josh Campion on 21/02/2016.
//  Copyright © 2016 Josh Campion Development. All rights reserved.
//

import Foundation

private var ButtonLocalizationKey:UInt8 = 0

extension UIButton: Localizable {
    
    @IBInspectable
    public var localizationKeyID:String? {
        get {
            return objc_getAssociatedObject(self, &ButtonLocalizationKey) as? String
        }
        set {
            
            objc_setAssociatedObject(self, &ButtonLocalizationKey, newValue, .OBJC_ASSOCIATION_RETAIN)
            
            if let keyID = newValue,
                let key = LocalizationKey(rawValue: keyID) {
                    
                self.setTitle(LocalizedString(key), for: UIControl.State())
                    
            }
        }
    }
}
