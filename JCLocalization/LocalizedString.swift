//
// Localization.swift
//
// Created by JCLocalization.
//

import Foundation

public enum LocalizationKey: String {

    /**
    
     A testing string.
    
    - Note: Default Value:
        *This is a test.*
    
    */
    case Test

    /**
    
     A testing string for escaping.
    
    - Note: Default Value:
        *This is a 'string' with \"characters\" to escape! 5% of people will forget to care about this. URL's (http://www.joshcampion.co.uk) wont' need escaping, nor will single backslashes (\). *
    
    */
    case Escape


}

/// Class for accessing the JCLocaliztion bundle to reference the correct `.strings` file.
public class LocalizationClass { }

/**

 Machine generated function to load an `NSLocalizedString(...)` entry using a convenience enum.

 - parameter key: The key to fetch a string for. The key's `rawValue` is used as the key in the `.strings` file.

*/
public func LocalizedString(_ key:LocalizationKey) -> String {
    return Bundle(for: LocalizationClass.self).localizedString(forKey: key.rawValue, value: nil, table: "JCLocalization")
}

