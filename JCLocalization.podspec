Pod::Spec.new do |s|
  s.name = 'JCLocalization'
  s.version = '0.4'
  s.summary = 'User facing text management made easy.'
  s.homepage     = "https://github.com/thedistance"
  s.license      = "MIT"
  s.author       = { "The Distance" => "dev@thedistance.co.uk" }
  s.platform     = :ios
  s.source       = { :git => "https://bitbucket.org/thedistance/jclocalization.git", :tag => "#{s.version}" }
  s.swift_version = '4.2'


  s.ios.deployment_target = '9.0'

  s.source_files = 'JCLocalization/**/*.swift'
  s.resources = 'JCLocalization/*.strings'
  s.preserve_paths = "JCLocalization.sh"

  s.requires_arc = true
end
